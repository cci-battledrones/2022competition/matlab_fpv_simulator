function plotGateDetailed(corners, label)
%   =======================================================================
%   
%   Description:
%   This function plots a generic 3D representation of a gate for the 
%   BattleDrone's obstacle course. This function can be replaced with 
%   'plotGateSimple' anywhere it appears. This function is for
%   visualization only and does not impact the results of the simulation.
%
%   Input Variables:
%       corners - a list of vectors pointing to the locations of a gate's corners
%              Size: [numCorners x dimension] 
%                    where numCorners is number of gate corners  
%                    and 'dimension' corresponds to the right, forward, and 
%                    up directions and will always have a length of 3.
%              Units: meters
%              Reference Frame: Map's Global RFU
%
%       label (optional) - an alphanumeric name of each gate to be printed 
%          above the gate in the 'fpvFigNum' figure 
%
%   Returned Variables:
%       NONE
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
global fpvFigNum  

    center = mean(corners,1);
    lookVector = [0 1 0];
    poseVector = calcPose(corners);
    angularOffset = calcAngleBetweenVectors (lookVector, -poseVector);
    width = 0.75;
    thickness = 0.5;

    gateType =  buildGate();  
    innerRing = gateType; 
    outerRing = [...
        innerRing(1,:) + [-width, 0, -width];     %Bottom left corner    
        innerRing(2,:) + [-width, 0,  width];     %Top left corner
        innerRing(3,:) + [ width, 0,  width];     %Top right corner
        innerRing(4,:) + [ width, 0, -width]];    %Bottom right corner
    frontSide = [innerRing;
                 outerRing];
    backSide =  frontSide +  [0, thickness, 0]; %Copy front side and offset it   

    Vertices = [frontSide; backSide]*rotateVector3(angularOffset) + center;       
    Faces =  [1,  5,  6,  2,  1;    %Front left panel
              2,  6,  7,  3,  2;    %Front top panel
              3,  7,  8,  4,  3;    %Front right panel
              4,  8,  5,  1,  4;    %Front bottom panel
              1,  2, 10,  9,  1;    %Inner left panel
              2,  3, 11, 10,  2;    %Inner top panel
              3,  4, 12, 11,  3;    %Inner right panel
              4,  1,  9, 12,  4;    %Inner bottom panel
              9, 13, 14, 10,  9;    %Back left panel
             10, 14, 15, 11, 10;    %Back top panel
             11, 15, 16, 12, 11;    %Back right panel
             12, 16, 13,  9, 12;    %Back bottom panel
              5,  6, 14, 13,  5;    %Outer left panel
              6,  7, 15, 14,  6;    %Outer top panel
              7,  8, 16, 15,  7;    %Outer right panel
              8,  5, 13, 16,  8];   %Outer bottom panel
    figure(fpvFigNum)
    patch('Faces',Faces,'Vertices',Vertices,'FaceColor','blue')   
    axis equal 
    if exist('label', 'var')
        text(center(1),center(2),center(3) + 1.5*width ,label,'FontSize',14)
    end
end

