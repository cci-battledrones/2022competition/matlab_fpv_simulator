function flyThroughGate(gateID, overshoot)
%   =======================================================================
%  
%   Description:
%   This function lets the drone fly through the given gate and overshoot
%   the gate's position by a specified distance. If the drone and gate are
%   not initially lined up, the drone re-approaches the gate.
%
%
%   Input Variables:
%       gateID - Gate identification number, created in 'buildCourse'. A
%           gate's identification number dictates what order the drone will fly
%           through the gate
%              Size: [1 x 1]
%       overshoot - Overshoot of the gate's position. This is done such
%           that the drone can be deposited a specified distance away from the
%           gate as to not have any risk of colliding with it as it flies to
%           the next one
%              Size: [1 x 1]
%              Units: meters
%
%   Returned Variables:
%       NONE
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
global estimatedDroneLocation estimatedDroneHeading

% Step 1: Align with gate
    gateLocation = getGateLocation(gateID);
    vector2Gate = gateLocation - estimatedDroneLocation;
    range = norm(vector2Gate);
    turnAngle = calcAngleBetweenVectors (estimatedDroneHeading, vector2Gate);
    turnDrone(turnAngle, calcRandomNoise (1,1))

% Step 2: if Drone and gate are poorly aligned approach gate again
    if abs(turnAngle) > 40
        offset = 10;
        approachGate(gateID, offset)
        vector2Gate = gateLocation - estimatedDroneLocation;
        range = norm(vector2Gate);
    end

% Step 3: Fly through the gate by the desired overshoot amount
    moveDrone(range + overshoot, calcRandomNoise (0.5,1))
    updateDrone()
end