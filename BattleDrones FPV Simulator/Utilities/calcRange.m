function range = calcRange(corners)
%   =======================================================================
%   
%   Description:
%       This function returns the range from the drone to the center of the
%       gate.
%
%   Input Variables:
%       corners - a list of vectors pointing to the locations of a gate's corners
%              Size: [numCorners x dimension] 
%                    where numCorners = number of gate corners  
%                    and dimension is always equal to 3
%              Units: meters
%              Reference Frame: Map's Global RFU
%
%   Returned Variables:
%       range - the range from the drone to the center of the gate
%              Size: [1 x 1]
%              Units: meters
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
global env
    figure(env)
    gateLocation = mean(corners,1);
    droneLocation = get(gca,'CameraPosition');
    range = norm(gateLocation - droneLocation); 
end