function range = estimateGateRange(projectedCorners)
%   =======================================================================
%  
%   Description:
%
%   This function estimates the distance to the gate's center from the
%   center of the drone. The function utilizes tilt angles and pulls
%   trigonometric identities from the functions in the Utilities folder to
%   estimate the drone's distance to center of the gate. The returned
%   distance is a scalar quantity with units of meters. 
%
%   Input Variables:
%
%       projectedCorners - Projected corners of the desired gate
%              Size: [numCorners x panTilt]
%                       'numCorners' corresponds to the number of corners (corner
%                       number, clockwise starting at lower left corner,
%                       created in 'buildGate'). Typically there will always
%                       be 4 corners.
%                       'panTilt' will always have a dimension of two. Dimension
%                       one corresponds to the pan angles and dimension two to
%                       the tilt angles.
%                       NOTE: The first column will always be the pan angle
%                       and the second will be tilt.
%              Units: degrees
%
%   Returned Variables:
%       range - a scalar quantity corresponding to the distance to the
%           gate's center from the center of the drone.
%              Size: [1 x 1]  
%              Units: meters
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

    %% Step 1: Calculate the distance to the left and right sides of the gate
    tiltAngles = projectedCorners(:,2);
    range1 = calcRangeFromTilt(tiltAngles(1), tiltAngles(2));
    range2 = calcRangeFromTilt(tiltAngles(4), tiltAngles(3));
    %% Step 2: Calculate the distance to the center of the gate
    range = (range1+range2)/2; 
end