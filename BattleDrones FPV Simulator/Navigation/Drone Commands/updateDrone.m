function updateDrone()
%   =======================================================================
%  
%   Description:
%   This function is called each time the drone moves or turns. It is used
%   to update the 'estimatedDroneLocation' and 'estimatedDroneHeading' based 
%   on any identified gate in view of the Drone's camera.
%
%   Input Variables:
%      NONE
%
%   Returned Variables:
%       NONE
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
global estimatedDroneLocation estimatedDroneHeading
       
% Step 1: Identify gates in drone's FOV
    [allProjectedCorners, gateIDs] = estimateGateIDs();
    numberOfGates = size(gateIDs,1);

% Step 2: Identify and localized based on the closest gate 
    if not(numberOfGates == 0)
        for ii = 1:numberOfGates
            ranges(ii) = estimateGateRange(allProjectedCorners(:,:,ii));
        end
        [~,closestGate] = min(ranges);
        [measuredDroneLocation, measuredDroneHeading] = orientDroneToMap(allProjectedCorners(:,:,closestGate), gateIDs(closestGate));
% Step 3: If the measured drone location is close to the previous estimate update the estimate        
        locationResiduals = measuredDroneLocation - estimatedDroneLocation;
        headingResidual = calcAngleBetweenVectors(measuredDroneHeading, estimatedDroneHeading);

        if norm(locationResiduals) < 10 && norm(headingResidual) < 10
            estimatedDroneLocation = measuredDroneLocation;
            estimatedDroneHeading = measuredDroneHeading;
        else
            
        end
    end
end