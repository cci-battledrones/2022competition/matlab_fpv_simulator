function  plotDrone()
%   =======================================================================
%   
%   Description:
%       This function updates the visualization of the drone's current location
%       within the course. It is expected that this function will be used 
%       to track the Drone's progress through the course and to review
%       the trajectory after a run.
%
%   Input Variables:
%       NONE
%
%   Returned Variables:
%       NONE
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================       
persistent  lastDroneLocation
% Step 1: plot the Drone's Current location on the FPV figure
    droneLocation = getDroneLocation;
    scatter3(droneLocation(1),droneLocation(2),droneLocation(3),'filled','b')
% Step 2: plot the Drone current look vector on the FPV figure
    lookVector = getDroneHeading;
    heading = [droneLocation; droneLocation + lookVector];
    line(heading(:,1),heading(:,2),heading(:,3),'Color','r')

% Step 3: If applicable, draw the path from the last location
    if not(isempty(lastDroneLocation))
        trail = [droneLocation; lastDroneLocation];
        line(trail(:,1),trail(:,2),trail(:,3),'Color','k')
    end
lastDroneLocation = droneLocation;
end

