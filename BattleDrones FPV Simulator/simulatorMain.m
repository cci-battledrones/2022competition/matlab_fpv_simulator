clear, close all, home
%   =======================================================================
%                           MAIN Driver Script
%   =======================================================================
%   
%   Description: simulatorMain.m combines all of the relevant functions
%   together in the manner they were intended to be used. The buildCourse
%   script sets up the simulated enviorment, buildDrone is used to
%   create a FPV drone that can be navigated through the course. The
%   subsection labeled "Autonomy" is the core of this work. The three
%   fundamental steps of Autonomous Drone Racing are walked through in the
%   "Autonomy" Section. Step 1: Find next gate, Step 2: Approach next gate,
%   and Step 3: Fly though next gate. Example functions have been built to
%   execute these three steps in a simple fashion. 
%
%   Can you do it better? Faster? Safer? More robust? Modify the code to
%   build a better autonomous drone.
%
%   Author: Kevin Schroeder, PhD
%           Research Scientist, Virginia Tech
%   Date:  31 May 2021
%
%   =======================================================================
%% Build and Setup
global pauseTime sideLength estimatedDroneLocation estimatedDroneHeading

%Access functions from all subfolder
addpath(genpath(pwd))

    %Initalize design variables
    pauseTime = 0.005; %sec (for visualization)
    sideLength = 1; %meters
    flightAltitude = sideLength/2 + 1;
    startingLocation = [25  -20   flightAltitude];
    startingDirection = [0,1,0];
    fovAngle = 30;

    % Build Simulation Environment
    buildCourse
    buildDrone(startingLocation + [calcRandomNoise(0.5,2), 0], ... %1/2 meter standard deviation error in x and y
                    startingDirection*rotateVector3(calcRandomNoise (5,1)),... %5 degree standard deviation error in pointing direction
                    fovAngle)
    pause(4*pauseTime)

    %% Initialize  Estimation
    estimatedDroneLocation = startingLocation;
    estimatedDroneHeading  = startingDirection;
    %% Autonomy
    objectives = [1:10]; % gate IDs
    %begin race
    tic
    for gateID = objectives
        fprintf('targeting Gate %i \n',gateID)
    %Step 1: Find next gate
    projectedCorners = findGate(gateID);
    %Step 2: Approach next gate
        offset = 4; %meters
        approachGate(gateID, offset)  
    %Step 3: Fly though next gate
        overshoot = 8; %meters
        flyThroughGate(gateID, overshoot)
    end
    timeSeconds = num2str(toc);
    fprintf('You completed the course in %s seconds \n', timeSeconds)
    % View from above
    set(gca,'CameraTarget',[0 30 0])
    set(gca, 'CameraViewAngle',50)
    set(gca,'CameraPosition',[0 30 80])