function gateLocation = getGateLocation(gateID)
%   =======================================================================
%   
%   Description:
%   This function pulls information from the MATLAB GLOBAL workspace. It
%   finds the mean position of a specific gate's corners (known a priori). 
%   Unlike 'estimateGateLocation', this function pulls the true gate
%   location from the map. This is further different as it reports the gate
%   location in the Global coordinate frame, measured from the map's origin
%   whereas 'estimateGateLocation' estimates the vector to the gate's
%   center from the drone in the Drone's local right-forward-up coordinate
%   frame (RFU)
%
%   It is expected that this function can be used to identify unknown gates 
%   or to derive the drone's current location based on its relative 
%   location to a known (i.e., identified) gate.
%
%   Input Variables:
%       gateID - Gate identification number, created in 'buildCourse'. A
%           gate's identification number dictates what order the drone will fly
%           through the gate
%              Size: [1 x 1]
%
%   Returned Variables:
%       gateLocation - a row vector to the center of the desired gate
%           from the map's origin
%              Size: [1 x dimension]
%                    'dimension' corresponds to the right, forward, and up directions;
%                    it will always be 3.
%              Units: Meters
%              Reference Frame: Map's Global RFU
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

    corners = getGateCorners(gateID);
    gateLocation = mean(corners,1);
end