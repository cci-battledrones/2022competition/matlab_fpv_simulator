function plotGateSimple(corners, label)
%   =======================================================================
%   
%   Description:
%   This function plots a generic 3D representation of a gate for the 
%   BattleDrone's obstacle course. This function can be replaced with 
%   'plotGateDetail' anywhere it appears. This function is for
%   visualization only and does not impact the results of the simulation.
%
%   Input Variables:
%       corners - a list of vectors pointing to the locations of a gate's corners
%              Size: [numCorners x dimension] 
%                    where numCorners is number of gate corners  
%                    and 'dimension' corresponds to the right, forward, and 
%                    up directions and will always have a length of 3.
%              Units: meters
%              Reference Frame: Map's Global RFU
%
%       label (optional) - an alphanumeric name of each gate to be printed 
%          above the gate in the 'fpvFigNum' figure 
%
%   Returned Variables:
%       NONE
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
global fpvFigNum  
    center = mean(corners,1);
    corners = [corners;corners(1,:)]; %repeat initial corner to make a close path

    figure(fpvFigNum)
    line(corners(:,1),corners(:,2),corners(:,3),...
        'LineJoin','round','LineWidth',2,'Marker','o',...
        'MarkerEdgeColor','k','MarkerFaceColor','green',...
        'MarkerSize',5)
    if exist('label', 'var')
        text(center(1),center(2),center(3)+1.2,label,'FontSize',14)
    end
end