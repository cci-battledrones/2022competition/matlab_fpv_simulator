function range = calcRangeFromTilt(tiltLow, tiltHigh)
%   =======================================================================
%   
%   Description:
%       This function calculates the horizontal distance from the drone to 
%       a gate by the measured angle between drone's current heading and 
%       the top of the gate and the bottom of the gate. Unlike 'calcRange',
%       this function does not return the length of the vector from the 
%       drone to the center of the gate. 
%
%   Input Variables:
%       tiltLow - the angle from the drone's local heading to the
%           bottom of the gate
%              Size: [1 x 1]
%              Units: degrees
%              Reference Frame: Drone's Local RFU 
%
%       tiltHigh - the angle from the drone's local heading to the
%           top of the gate
%              Size: [1 x 1]
%              Units: degrees
%              Reference Frame:  Drone's Local RFU 
%
%   Returned Variables:
%       range - the horizontal distance between the drone and the gate. 
%              Size: [1 x 1]
%              Units: meters
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   NOTE: This trig identity assumes the gates are perfectly vertical and
%         the drone is perfectly level.
%
%   =======================================================================
global sideLength
    range = sideLength/(tand(tiltHigh)-tand(tiltLow)); 
end