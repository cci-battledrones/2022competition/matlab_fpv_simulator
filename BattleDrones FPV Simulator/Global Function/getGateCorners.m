function corners = getGateCorners(gateID)
%   =======================================================================
%   
%   Description:
%   This function pulls information from the MATLAB GLOBAL workspace. It
%   finds the position of a specific gate's corners (known a priori). 
%
%   It is expected that this function can be used to identify unknown gates 
%   or to derive the drone's current location based on its relative 
%   location to a known (i.e., identified) gate.
%
%   Input Variables:
%       gateID - Gate identification number, created in 'buildCourse'. A
%           gate's identification number dictates what order the drone will fly
%           through the gate
%              Size: [1 x 1]
%
%   Returned Variables:
%       corners - A list of vectors pointing to the locations of a gate's 
%			corners. The corners are listed clockwise from the 
%			bottom left (as seen when looking at the front of the 
%			gate)
%              Size: [numCorners x dimension] 
%                    where 'numCorners' is number of gate corners  
%                    and 'dimension' corresponds to the right, forward, and 
%                    up directions and will always have a length of 3.
%              Units: meters
%              Reference Frame: Map's Global RFU

%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

global map
    corners = map(:,:,gateID);
end