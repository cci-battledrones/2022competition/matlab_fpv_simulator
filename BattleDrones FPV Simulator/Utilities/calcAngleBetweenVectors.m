function angle = calcAngleBetweenVectors (vector1, vector2, check)
%   =======================================================================
%   
%   Description:
%       This function calculates the angle between two vectors. The
%       function will either use an optionally provided quadrant check or
%       will calculate its own. If no quadrant check is given, the check
%       is determined by assuming CW and CCW rotation is determined based
%       on the local z-axis.
%
%   Input Variables:
%       vector1 - the first vector
%              Size: [1 x 3]
%       vector2 - the second vector
%              Size: [1 x 3]
%       check (optional) - the quadrant check (+1 or -1) 
%              Size: [1 x 1]
%              Units: unitless
%
%   Returned Variables:
%       angle - the absolute angle between the given vectors
%              Size: [1 x 1]
%              Units: degrees
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   NOTE: This function utilizes 'acosd' which has a domain of [0°, 180°].
%         A quadrant check (+1 or -1) is added to expand the domain to
%         [-180°, +180°].
%   =======================================================================

% Step 1: If no quadrant check is provided, assume rotation around z-axis
    if not(exist('check', 'var')) 
        rotation = cross(vector1,vector2);
        if rotation(3) == 0
            check = 1;
        else
            upVector = [0, 0, 1];
            check = sign(dot(upVector,rotation));
         end
    end

% Step 2: Throw warning if check is not +1 or -1
    if norm(check)~= 1, check = sign(check); end

% Step 3: Use the definition of a dot product to solve for the angle between two vectors
    cosAngle = dot(vector1,vector2)/(norm(vector1)*norm(vector2));
    angle = check*acosd(cosAngle);
    angle = real(angle); %rounding error can sometimes cause a small imaginary component.
end