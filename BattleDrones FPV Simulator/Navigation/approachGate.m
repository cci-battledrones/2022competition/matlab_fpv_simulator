function approachGate(gateID, offset)
%   =======================================================================
%  
%   Description:
%   This function will direct the drone to a desired gate. It first will
%   set an objective on the course map, then fly towards that objective,
%   and align itself with the gate.
%
%   Input Variables:
%       gateID - Gate identification number, created in 'buildCourse'. A
%           gate's identification number dictates what order the drone 
%           will fly through the gate
%              Size: [1 x 1]
%       offset - Desired distance in front of the gate that you want the
%           drone to travel to.
%              Size: [1 x 1]
%              Units: meters
%
%   Returned Variables:
%       NONE
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
global estimatedDroneLocation estimatedDroneHeading
%step 1: set objective on map
    gateLocation = getGateLocation(gateID);
    gatePose = getGatePose(gateID);
    objective = gateLocation + offset*gatePose;
%step 2: go to objective
    range = norm(objective - estimatedDroneLocation);
    desiredHeading = (objective - estimatedDroneLocation)/norm(objective - estimatedDroneLocation);
    turnAngle = calcAngleBetweenVectors (estimatedDroneHeading, desiredHeading);
    turnDrone(turnAngle, calcRandomNoise (5,1))
    moveDrone(range, calcRandomNoise (0.5,1))
%step 3: align with gate
    turnAngle = calcAngleBetweenVectors (estimatedDroneHeading, -gatePose);
    turnDrone(turnAngle, calcRandomNoise (1,1))
    updateDrone()
% Step 4: ensure Target Gate is in view   
    isVisible = 0;
    while not(isVisible)
        [~, gateIDs] = estimateGateIDs;
        isVisible = any(gateIDs == gateID);
        if not(isVisible)
            moveDrone(-5,calcRandomNoise (0.5,1))
            updateDrone()
        end
    end
end