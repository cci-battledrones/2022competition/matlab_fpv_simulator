function noise = calcRandomNoise (magnitude,dimension)
%   =======================================================================
%   
%   Description:
%       This function returns a list of random numbers generated from a  
%       normal distribution with a standard deviation prescribed by 
%       'magnitude'. The length of the list is dictated by 'dimension'.
%  
%       It is expected that this function will be used to generate random
%       zero-mean Gaussian noise. 
%
%   Input Variables:
%       magnitude - the standard deviation of the noise 
%              Size: [1 x 1]
%       dimension - determines the length of the [1 x n] output array
%              Size: [1 x 1]
%       
%   Returned Variables:
%       noise - an array of random noise scaled by the given magnitude
%           and of the given length
%              Size: [1 x n] 
%                   where n = dimension
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

    noise = magnitude * randn(1,dimension);
end