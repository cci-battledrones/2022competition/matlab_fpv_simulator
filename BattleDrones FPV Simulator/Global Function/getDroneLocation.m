function droneLocation = getDroneLocation()
%=======================================================================
%   
%   Description:
%   This function pulls information about the first person view figure. It 
%   sets the current axes to be the FPV figure saved in the global workspace. 
%
%   It is expected that this function can be used to support functions from
%   the "Simulate Camera", "Visualization", and "Navigation" folders, 
%   especially in the process of calculating how/if the FPV camera on the
%   drone sees objects in its current frame. But it may have other uses as
%   well.
%
%   Input Variables:
%       NONE
%
%   Returned Variables:
%       droneLocation  -   a vector of the drone's current location, points 
%                           from the origin of the map to the Drone. 
% 
%   NOTE: the returned vector is in the Global coordinate frame
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
global fpvFigNum
    droneLocation = get(gca(fpvFigNum),'CameraPosition');
end