function poseVector = calcPose(corners)
%   =======================================================================
%   
%   Description:
%       This function returns a unit vector pointing in the normal
%       direction of a gate entrance.
%
%   Input Variables:
%       corners - a list of vectors pointing to the locations of a gate's corners
%              Size: [numCorners x dimension] 
%                    where numCorners is the number of gate corners  
%                    and dimension is always equal to 3
%              Units: meters
%              Reference Frame: Map's Global RFU
%
%   Returned Variables:
%       poseVector - a unit vector normal to the plane of the gate pointing
%           out the front of the gate.
%              Size: [1 x 3]
%              Units: meters
%              Reference Frame: Map's Global RFU
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   NOTE: By default the corners are numbered CW with the first corner in
%         the lower left when viewing from in front of the gate.
%
%   =======================================================================

    vec1 = corners(3,:)-corners(1,:);
    vec2 = corners(2,:)-corners(4,:);
    pose = cross(vec1,vec2);
    poseVector = pose/norm(pose);
end