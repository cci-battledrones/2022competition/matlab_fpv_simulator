function [droneGlobalLocation, droneGlobalOrientation] = orientDroneToMap(projectedCorners, gateID)

%   =======================================================================
%  
%   Description:
%   This function calculates where the drone's location on the course map 
%   based on its relationship to a known gate in its field of view. This
%   function assigns the provide gateID to the projectedCorners, and 
%   combines it with information from the MATLAB GLOBAL workspace to estimate
%   the drone's location and orientation on the course map. 
%   Unlike 'getDroneLocation' and 'getDroneHeading,' this function is only 
%   an estimate. If the gateID provided does not correspond to the 
%   projectedCorners the resulting location and heading will be erroneous.
%
%   Input Variables:
%       projectedCorners - Projected corners of the gate
%              Size: [numCorners x panTilt]
%                       'numCorners' corresponds to the number of corners (corner
%                       number, clockwise starting at lower left corner,
%                       created in 'buildGate'). Typically there will always
%                       be 4 corners.
%                       'panTilt' will always have a dimension of two. Dimension
%                       one corresponds to the pan angles and dimension two to
%                       the tilt angles.
%                       NOTE: The first column will always be the pan angle
%                       and the second will be tilt.
%              Units: degrees
%       gateID - Gate identification number, created in 'buildCourse'. A
%           gate's identification number dictates what order the drone will fly
%           through the gate
%              Size: [1 x 1]
%
%   Returned Variables:
%       droneGlobalLocation - a vector of the drone's current location, points 
%           from the origin of the map to the drone. 
%              Size: [1 x dimension]
%                    'dimension' corresponds  to the right, forward, and up directions;
%                    it will always be 3.
%              Units: Meters
%              Reference Frame: Map's Global RFU
%       droneGlobalOrientation - a unit vector of the drone's orientation, points 
%           along the FPV camera's boresight
%              Size: [1 x dimension]
%                    'dimension' corresponds  to the right, forward, and up directions;
%                    it will always be 3.
%              Units: unitless
%              Reference Frame: Map's Global RFU
%
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

%Step 1: Estimate drone's location relative to a gate
    %NOTE: The "Relative" vector will be in a gate-based coordinate frame
    [droneRelativeLocation, droneRelativeOrientation] = orientDroneToGate(projectedCorners);
%Step 2: Look up the gate's location and orinitation on the map
    %NOTE: The Gate's location and orinitation from the map will be in the
    %"Global" coordinate frame
    gateGlobalLocation = getGateLocation(gateID);
    gatePose = getGatePose(gateID);
    gateGlobalOrientation = calcAngleBetweenVectors([0 1 0],gatePose);
%Step 3: Estimate drone's location and orientation on the map
    %Note1: Because `droneRelativeLocation` is not in the "Global" frame it
    %must be rotated before it can be added to 'gateGlobalLocation'
    
    %Note2: 'gateGlobalOrientation' is the angle from the "Global" forward
    %direction to the gate's forward direction. The opposite angle can be
    %used to make a rotation matrix from the Gate "Relative" frame to the
    %"Global" frame
    gate2Map = rotateFrame3(-gateGlobalOrientation);
    droneGlobalLocation = gateGlobalLocation + droneRelativeLocation*gate2Map;
    droneGlobalOrientation = droneRelativeOrientation*gate2Map;
end