function buildCourse()
%   =======================================================================
%   
%   Description:
%   This function generates a 3D environment that is representative of the 
%   BattleDrone's obstacle course in a MATLAB figure. The 3D environment is
%   for a FPV simulation where the camera of MATLAB axes can be moved and 
%   oriented to emulate the motion of FPV drone. This function plots the 
%   VT Drone Park and places the 10 gates in their competition locations.
%   The figure handle is saved in the MATLAB global workspace. 
%
%   Input Variables:
%       NONE
%
%   Returned Variables:
%       NONE
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

global map sideLength fpvFigNum
% Step 1: Place gates on the course
    %create gate template
    gateType =  buildGate();            

    %create course template (rotate and translate)
    gates(:,:,1) = gateType + [25,-10,sideLength/2 + 1];
    gates(:,:,2) = gateType + [25,10,sideLength/2 + 1];
    gates(:,:,3) = gateType + [-10,30,sideLength/2 + 1];
    gates(:,:,4) = gateType*rotateVector3(330) + [25,50,sideLength/2 + 1];
    gates(:,:,5) = gateType*rotateVector3(30)  + [25,65,sideLength/2 + 1];
    gates(:,:,6) = gateType*rotateVector3(150) + [-25,65,sideLength/2 + 1];
    gates(:,:,7) = gateType*rotateVector3(210) + [-25,50,sideLength/2 + 1];
    gates(:,:,8) = gateType*rotateVector3(180) + [10,30,sideLength/2 + 1];
    gates(:,:,9) = gateType*rotateVector3(180) + [-25,10,sideLength/2 + 1];
    gates(:,:,10)= gateType *rotateVector3(180)+ [-25,-10,sideLength/2 + 1];


% Step 2: Create the visual environment by plotting the course with gates
    fpvFigNum = 99;
    figure(fpvFigNum);
    hold on
    plotDroneCage
    for ii = 1:length(gates)
        plotGateDetailed(gates(:,:,ii),num2str(ii))
    end
    view(30,15)

% Step 3: Create a map of gate locations in the GLOBAL Workspace
    map = gates;
end