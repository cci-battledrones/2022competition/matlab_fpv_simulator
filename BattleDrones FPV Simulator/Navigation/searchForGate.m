function searchForGate(gateID)
%   =======================================================================
%  
%   Description:
%   If given a gate identification number, this function rotates the drone's 
%   heading to match that of the direction it expects to find the gate.
%   Otherwise, it simply rotates 10 degrees counter clockwise.
%
%   Input Variables:
%       gateID - Gate identification number, created in 'buildCourse'. A
%           gate's identification number dictates what order the drone will fly
%           through the gate
%              Size: [1 x 1]
%
%   Returned Variables:
%       NONE
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
% Step 1: if no gateID is specified, set turn angle to 10 deg
    if not(exist('gateID', 'var')) 
        turnAngle = 10;
        
% Step 2: if a gateID is specified, calculate the turn angle required to point at the desired gate
    else
        gateLocation = getGateLocation(gateID);
        turnAngle = estimatePanTilt(gateLocation);
    end

% Step 3: Turn the drone
    turnDrone(turnAngle, calcRandomNoise (1,1))
    updateDrone();
end
