function [pan, tilt] = getPanTilt(points)
%   =======================================================================
%   
%   Description:
%   This function pulls information about the MATLAB axes (i.e.,
%   the MATLAB figure). 
%
%   It is expected that this function can be used to support functions from
%   the "Simulate Camera" folder, especially in the process of calculating 
%   the pan and tilt angles of gate corners, but may have other uses as
%   well.
%
%   Input Variables:
%       point   - the position row vector from the map's origin to the 
%                   desired point
%       NOTE: the input vector should be in the Global coordinate frame
%
%   Returned Variables:
%       pan - the angle (in degrees) across the drone's local horizon
%           (i.e., the Right-Front plane) from the drone's current 
%           'lookVector' to the a projection of the 'point' on to
%           the drone's local horizon. Positive angles are CCW 
%		    rotations (defined by the Right-hand Rule).
%               Size: ['numPoints' x 1]
%                   'numPoints' corresponds to the number of points 
%               Units: degrees

%
%       tilt    - the angle (in degrees) up or down from the drone's local 
%                 horizon to the provided 'point'
% 
%   NOTE: Following the right-hand convention, clockwise rotations are 
%   negative. Therefore, pan angles are negative if the 'point' is right of
%   the FPV camera and tilt angles are negative if the 'point' is below the
%   FPV camera (when viewed from the camera)
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

    droneLocation = getDroneLocation;
    lookVector = getDroneHeading;

    numPoints = size(points,1);
    pan = zeros(numPoints,1);
    tilt = zeros(numPoints,1);
    for ii = 1:numPoints
        directionVector = (points(ii,:) - droneLocation)/norm(points(ii,:) - droneLocation);
        [pan(ii), tilt(ii)] = calcPanTilt(lookVector, directionVector);
    end
end