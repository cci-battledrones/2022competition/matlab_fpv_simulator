function turnDrone(turnAngle, error)
%   =======================================================================
%  
%   Description:
%   This function alters the drone's heading by a specified angle. The 
%   drone turns by the step size until the specified angle is turned. Some 
%   error can be optionally added so the model can be tested under more 
%   realistic conditions (e.g., things like wind or imperfect sensor 
%   readings will throw off your drone's commanded movements).
%
%   Input Variables:
%       turnAngle - The angular distance you would like the drone to rotate.
%              Size: [1 x 1]
%              Units: degrees
%       error (optional) - error to be added into the angle the drone is
%           to turn.
%              Size: [1 x 1]
%              Units: degrees
%
%   Returned Variables:
%       NONE
%
%   NOTE: The drone will rotate clockwise if the given turn angle (plus
%         error) is negative
%
%   NOTE: The simulated drone's heading is not changed all at once to give the visualization 
%         a smoother transition.
%
%   NOTE: The updated 'estimatedDroneHeading' does not included the added error because the
%         error is unknown. This will cause the 'estimatedDroneHeading' to drift from the 
%         true 'lookVector' each time the drone turns.
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
global pauseTime estimatedDroneHeading fpvFigNum

% Step 1: If no noise is provided, set default to zero
    if not(exist('error', 'var')) 
        error = 0;
    end

% Step 2: Incrementally turn the camera the desired angle
    direction = sign(turnAngle + error);
    step = direction*1;
    for ii = step:step:(turnAngle + error)
        droneLocation = getDroneLocation;
        lookVector = getDroneHeading*rotateVector3(step); 
        set(gca(fpvFigNum),'CameraTarget',droneLocation + lookVector)
        
        pause(pauseTime)
    end

% Step 3: Update GLOBAL data
    estimatedDroneHeading = estimatedDroneHeading*rotateVector3(turnAngle);
end