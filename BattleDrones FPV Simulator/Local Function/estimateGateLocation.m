function gateRelativeLocation = estimateGateLocation(projectedCorners)
%   =======================================================================
%  
%   Description:
%
%   This function estimates the vector to the gate's center from the 
%   center of the drone. The function references the 'estimateGateRange' 
%   function from the Local Function folder for calculating the magnitude 
%   of the vector and it references the 'calcVectorFromAngles' function
%   from the Utilities folder for calculating the direction of the vector.
%   The returned location vectors is in the Drone's local right-forward-up 
%   coordinate frame (RFU).
%
%   Input Variables:
%
%       projectedCorners - Projected corners of the desired gate
%              Size: [numCorners x panTilt]
%                       'numCorners' corresponds to the number of corners (corner
%                       number, clockwise starting at lower left corner,
%                       created in 'buildGate'). Typically there will always
%                       be 4 corners.
%                       'panTilt' will always have a dimension of two. Dimension
%                       one corresponds to the pan angles and dimension two to
%                       the tilt angles.
%                       NOTE: The first column will always be the pan angle
%                       and the second will be tilt.
%              Units: degrees
%
%   Returned Variables:
%       gateRelativeLocation - A vector from the drone to the center of the gate
%              Size: [1 x dimension]
%                    'dimension' corresponds to the right, forward, and up directions;
%                    it will always be 3.
%              Units: Meters
%              Reference Frame: Drones's Local RFU
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

    %% Step 1: Calculate mean pan and tilt angles of the corners
    panAngles = projectedCorners(:,1);
    meanPan = mean(panAngles,1); 
    
    tiltAngles = projectedCorners(:,2);
    meanTilt = mean(tiltAngles,1);
    %% Step 2: Calculate the distance to the center of the gate     
    range = estimateGateRange(projectedCorners);
    %% Step 3: Calculate the vector to the center of the gate    
    gateRelativeLocation = range * calcVectorFromAngles(meanPan,meanTilt);
end