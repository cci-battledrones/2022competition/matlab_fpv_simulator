function vector = calcVectorFromAngles(pan,tilt)
%   =======================================================================
%   
%   Description:
%       This function creates a unit vector representation of pan and tilt
%       angles referenced from the drone's current look vector
%
%   Input Variables:
%       pan - the angle (in degrees) across the drone's local horizon
%           (i.e., the Right-Front plane) from the drone's current 
%           'lookVector' to the a projection of the 'point' on to
%           the drone's local horizon. Positive angles are CCW 
%		rotations (defined by the Right-hand Rule).
%               Size: ['numPoints' x 1]
%                   'numPoints' corresponds to the number of points 
%               Units: degrees
%
%       tilt - The angle in the z plane between the desired point and the
%           drone's current look vector
%              Size: [1 x 1]
%              Units: degrees
%
%   Returned Variables:
%       vector - the normal vector representation of the given angles centered on
%           the drone's current look vector
%              Size: [1 x 3]
%              Units: unitless
%              Reference Frame: Drone's Local RFU
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

    lookVector = [0 1 0];
    vector = lookVector*rotateVector1(tilt)*rotateVector3(pan);
end