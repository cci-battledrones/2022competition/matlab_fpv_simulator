function plotDroneCage()
%   =======================================================================
%   
%   Description:
%   This function generates a generic 3D representation of the VT Drone 
%   Park in the 'fpvFigNum' figure. The 'fpvFigNum' is the figure number 
%   saved in the MATLAB global workspace and is created in 'buildCourse.'
%
%   Input Variables:
%       NONE
%
%   Returned Variables:
%       NONE
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
global fpvFigNum

    figure(fpvFigNum)
    width = [-50,50]; %meters
    depth = [-20,80]; %meters
    height = [0,50];  %meters
    numRibs = 6;

    for ii = 1:numRibs
        cage = [width(1),diff(width)/(numRibs-1)*(ii-2),height(1);
            width(1),diff(width)/(numRibs-1)*(ii-2),height(2);
            width(2),diff(width)/(numRibs-1)*(ii-2),height(2);
            width(2),diff(width)/(numRibs-1)*(ii-2),height(1)];
        line(cage(:,1),cage(:,2),cage(:,3),...
            'LineJoin','round','LineWidth',4,'Color','k') %plot cage ribs
    end

    surf([width(1),width(2);width(1),width(2)],...
        [depth(1),depth(1);depth(2),depth(2)],...
        [height(1),height(1);height(1),height(1)])
    axis equal
    xlim(width)
    ylim(depth)
    zlim(height)
end