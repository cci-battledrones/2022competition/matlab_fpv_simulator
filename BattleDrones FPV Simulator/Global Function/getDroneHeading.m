function lookVector = getDroneHeading()
%   =======================================================================
%   
%   Description:
%   This function pulls information about the first person view figure. It 
%   sets the current axes to be the FPV figure saved in the global workspace. 
%
%   It is expected that this function can be used to support functions from
%   the "Simulate Camera", "Visualization", and "Navigation" folders, 
%   especially in the process of calculating how/if the FPV camera on the
%   drone sees objects in its current frame. But it may have other uses as
%   well.
%
%   Input Variables:
%       NONE
%
%   Returned Variables:
%       lookVector  -   a unit vector of the drone's orientation, points 
%                       along the FPV camera's boresight
%
% 
%   NOTE: the returned vector is in the Global coordinate frame
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
global fpvFigNum  
    lookVector = get(gca(fpvFigNum),'CameraTarget') - get(gca(fpvFigNum),'CameraPosition');
end
