function rotationMatrix = rotateVector1(deg)
%   =======================================================================
%   
%   Description:
%       This function returns a vector rotation matrix meant to be used to
%       rotate a vector about the x axis by a certain number of degrees.
%       Unlike 'rotateFrame1', this matrix changes the original vector into 
%       a new vector (e.g. a heading vector can be rotated 30° to a new 
%       heading when viewed from the global frame).
%
%   Input Variables:
%       deg - the number of degrees to rotate about the x axis
%              Size: [1 x 1]
%              Units: degrees
%
%   Returned Variables:
%       rotationMatrix - the rotation matrix necessary to rotate
%           a vector about the x axis by the input number of degrees
%              Size: [3 x 3]
%              Units: unitless
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   NOTE: The returned variable should be used in the following mathematical 
%       expression: [VECTOR_2] = [VECTOR_1] * [rotationMatrix]
%       where VECTOR_1 and VECTOR_2 are [1 x 3] vectors.
%
%   NOTE: For more information on this, look up "Vector Rotation" in any 
%       linear algebra textbook or at: 
%       https://en.wikipedia.org/wiki/Rotation_matrix 
%   =======================================================================

    rotationMatrix = [1     0       0; 
                  0 cosd(deg)  sind(deg); 
                  0 -sind(deg) cosd(deg)];
end