function [allProjectedCorners, gateIDs, unassigned] = estimateGateIDs()
  %   =======================================================================
%   Description:
%   This function looks at the visible gates using the 'getFrame()' 
%   function which emulates the output from the onboard computer vision. 
%   The function assigns gateIDs to the visible gates using the Munkres 
%   algorithm and returns the assigned, unassigned gateIDs and the pan 
%   and tilt angles to the corners of all visible gates. The pan and tilt 
%   angles of the corners of each visible gate are associated with their
%   corresponding gateID.
%
%   The function pulls the map information from the MATLAB GLOBAL workspace
%   to compare the estimated location of the visible gates to the predicted
%   location of each gate on the map. To compare the location of gates, the
%   function compares the estimated and predicted range and pan angle of the
%   gates.
%
%   Input Variables:
%       NONE
%
%   Returned Variables:
%
%       allProjectedCorners - a 3 dimensional matrix, corresponding to the
%           pan and tilt angles of the corners of all visible gates. Angles
%           are measured from the drone's 'look vector' to each gate corner.
%           Each page of the matrix corresponds to each visible gate and is the 
%           row index of 'gateIDs' for the associated gate. The rows of each
%           page correspond to the number of corners and the first and second 
%           columns of each page contain the pan and tilt angles of all corners
%           respectively. 
%
%           Example: allProjectedCorners(:,1,2) returns pan angles of all 
%                corners of the second visible gate
%
%           Example: allProjectedCorners(:,2,4) returns tilt angles of all 
%                 corners of the fourth visible gate.
%
%              Size: [numCorners x numAngles x numberOfGates]
%                       'numCorners' corresponds to the number of corners
%                                    and will always be equal to 4
%                       'numAngles' corresponds to the pan and tilt angles
%                                   respectively and is always equal to 2     
%                       'numberOfGates' corresponds to the number of number
%                                       of visible gates
%              Units: degrees
%
%       gateIDs - a column vector corresponding to the gateID of the 
%           visible gates. The row index of each value in gateIDs
%           corresponds to the associated gate in 'allProjectedCorners'
%               Size: [numberOfGates x 1]
%                       'numberOfGates' corresponds to the number of 
%                       visible gates
%               Example: allProjectedCorners(:,:,gateIDs==3) returns the 
%                          pan and tilt angles of all corners of the gate
%                          with the gateID of 3.
%
%       unassigned - a column vector corresponding to the 
%           unassigned gateIDs
%
%               Size: [numberOfGates x 1]
%                       'numberOfGates' corresponds to the number of number
%                                       of visible gates
%
%   NOTE: The number of assigned and unassigned gateIDs will always add up
%         to 10, which corresponds to the total number of gates on the map.
%
%   NOTE: To learn more about the Munkres algorithm, type 
%         'help assignmunkres' into MATLAB's Command Window or follow the 
%          examples on mathworks.com at:
%         'https://www.mathworks.com/help/fusion/ref/assignmunkres.html'
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
    
% Step 1 Predict: Use the map to predict where gates should be
global map
    for gateID = 1:size(map,3)
        corners = getGateCorners(gateID);
        numCorners = size(corners,1);
        predictedPanTilts = zeros(numCorners,2);
        for ii = 1:numCorners
            [predictedPanTilts(ii,1), predictedPanTilts(ii,2)] = estimatePanTilt(corners(ii,:));
        end
        predictedRange(gateID) = estimateGateRange(predictedPanTilts);
        predictedPan(gateID) = mean(predictedPanTilts(:,1));
    end

% Step 2 Measure: Measure where gates are
    measuredPanTilts = getFrame(); 
    numberOfGates = size(measuredPanTilts,3);

    if not(numberOfGates == 0)
        for gate = 1:numberOfGates
            measuredRange(gate) = estimateGateRange(measuredPanTilts(:,:,gate));
            measuredPan(gate) = mean(measuredPanTilts(:,1,gate));
        end
% Step 3 Correlate: predictedCorners and measuredCorners
        prediction = [predictedRange',predictedPan'];
        measurements = [measuredRange',measuredPan'];

        for jj = 1:size(prediction, 1)
            delta = measurements - prediction(jj, :);
            costMatrix(jj, :) = sqrt(sum(delta.^2, 2));
        end
        costOfNonassignment = 15;

        [assignments, ~, unassigned] = assignmunkres(costMatrix,costOfNonassignment);
        gateIDs = assignments(:,1);
    else
        gateIDs = [];
        unassigned = 1:10;
    end
    allProjectedCorners = measuredPanTilts;
end
        
        