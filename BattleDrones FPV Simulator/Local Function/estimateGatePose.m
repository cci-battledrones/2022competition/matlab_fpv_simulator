function poseVector = estimateGatePose(projectedCorners)
%   =======================================================================
%  
%   Description:
%
%   This function estimates the gate's normal vector which corresponds to 
%   the unit vector of the gate's orientation in the Drone's local 
%   right-forward-up coordinate frame (RFU). This vector points out of the 
%   front side of the gate. The function references the functions in the 
%   Utilities folder to calculate the range and direction to the left and 
%   right side of the gate, and utilizes trigonometric identities to 
%   estimate gate's normal vector.
%
%   Input Variables:
%
%       projectedCorners - Projected corners of the desired gate
%              Size: [numCorners x panTilt]
%                       'numCorners' corresponds to the number of corners (corner
%                       number, clockwise starting at lower left corner,
%                       created in 'buildGate'). Typically there will always
%                       be 4 corners.
%                       'panTilt' will always have a dimension of two. Dimension
%                       one corresponds to the pan angles and dimension two to
%                       the tilt angles.
%                       NOTE: The first column will always be the pan angle
%                       and the second will be tilt.
%              Units: degrees
%
%   Returned Variables:
%       poseVector - a unit vector normal to the plane of the gate. The 
%           vector points towards the viewer when viewed from the front
%           of the gate
%              Size: [1 x dimension]
%                    'dimension' corresponds to the right, forward, and up directions;
%                    it will always be 3.
%              Units: unitless
%              Reference Frame: Drone's Local RFU
%
%   NOTE: A range vector is calculated as: rangeVec = rangeMag*rangeUnit
%         where rangeMag is the magnitude of the vector and rangeUnit is
%         its direction.
%   NOTE: Vec/norm(Vec) calculates a unit vector that carries the direction
%         of Vec
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

    %% Step 1: Estimate vector to the left and right side of the gate
    panAngles = projectedCorners(:,1);
    tiltAngles = projectedCorners(:,2);
    rangeMag1 = calcRangeFromTilt(tiltAngles(1), tiltAngles(2));
    rangeUnit1 = calcVectorFromAngles(mean(panAngles(1:2)),mean(tiltAngles(1:2)));
    rangeVec1 = rangeMag1*rangeUnit1;
    
    rangeMag2 = calcRangeFromTilt(tiltAngles(4), tiltAngles(3));
    rangeUnit2 = calcVectorFromAngles(mean(panAngles(3:4)),mean(tiltAngles(1:2)));
    rangeVec2 = rangeMag2*rangeUnit2;
    %% Step 2: Find the vector that goes from the left to right side of the gate
    left2Right = (panAngles(1)>panAngles(4))*2-1;
    %NOTE: left2Right will be +1 if it is rangeVec1 points to the left side
    %      and -1 if rangeVec1 points to the right side (from the drone's perspective).
    gateWidthVector = left2Right*(rangeVec2 - rangeVec1);
    %% Step 3: Derive the pose vector of the gate
    upVector = [0, 0, 1];
    poseVector = cross(gateWidthVector, upVector)/norm(gateWidthVector);
end