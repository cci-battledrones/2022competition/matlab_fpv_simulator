function moveDrone(moveDistance, error)
%   =======================================================================
%  
%   Description:
%   This function moves the drone forward by a specified distance. 
%   The drone moves forward by the defined step size until the specified 
%   distance is traversed. Some error can be optionally added so the model 
%   can be tested under more realistic conditions (e.g., things like wind 
%   or imperfect sensor readings will throw off your drone's commanded 
%   movements).
%
%   Input Variables:
%       moveDistance - The distance you want the drone to move forward. If
%           given a negative number, the drone will move backwards.
%              Size: [1 x 1]
%              Units: meters
%       error (optional) - error to be added into the distance the drone is
%           to move.
%              Size: [1 x 1]
%              Units: meters
%
%   Returned Variables:
%       NONE
%
%   NOTE: The simulated drone's position is not moved all at once to give the visualization 
%         a smoother movement.
%
%   NOTE: The updated 'estimatedDroneLocation' does not included the added error because the
%         error is unknown. This will cause the 'estimatedDroneLocation' to drift from the 
%         true 'droneLocation' each time the drone moves.
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
global pauseTime estimatedDroneLocation estimatedDroneHeading fpvFigNum

% Step 1: If no noise is provided, set default  to zero
    if not(exist('error', 'var')) 
        error = 0;
    end

% Step 2: Incrementally move the camera the desired distance
    direction = sign(moveDistance + error);
    step = direction*0.25;
    for ii = step:step:moveDistance + error
        droneLocation = get(gca,'CameraPosition');
        lookVector = getDroneHeading;
        droneNewLocation = droneLocation + lookVector*step;
        set(gca(fpvFigNum),'CameraPosition', droneNewLocation)
        set(gca(fpvFigNum),'CameraTarget', droneNewLocation + lookVector)
        
        pause(pauseTime)
    end

% Step 3: Update GLOBAL data
    estimatedDroneLocation = estimatedDroneLocation + moveDistance*estimatedDroneHeading;
    plotDrone()
end

