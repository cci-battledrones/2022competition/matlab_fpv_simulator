function rotationMatrix = rotateFrame3(deg)
%   =======================================================================
%   
%   Description:
%       This function returns a frame rotation matrix meant to be used to
%       rotate a frame about the z axis by a certain number of degrees.
%       Unlike 'rotateVector3', this matrix does not change one vector 
%       into another vector. This matrix changes how a single vector would 
%       be measured when viewed from a new reference frame (e.g. a gate pose
%       vector can be measured in the global frame of the MAP or in a local
%       frame relative to the moving drone).
%
%   Input Variables:
%       deg - the number of degrees to rotate the frame about the z axis
%              Size: [1 x 1]
%              Units: degrees
%
%   Returned Variables:
%       rotationMatrix - the rotation matrix necessary to rotate
%           a frame about the z axis by the input number of degrees
%              Size: [3 x 3]
%              Units: unitless
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   NOTE: The returned variable should be used in the following mathematical 
%       expression: [VECTOR_frame2] = [VECTOR_frame1] * [rotationMatrix]
%       where VECTOR_frame1 and VECTOR_frame2 are [1 x 3] vectors.
%
%   NOTE: For more information on this, look up "Change of Basis" in any 
%       linear algebra textbook or at: 
%       https://en.wikipedia.org/wiki/Change_of_basis
%   =======================================================================

    rotationMatrix = [cosd(deg) -sind(deg) 0; 
                  sind(deg) cosd(deg) 0; 
                  0         0         1];
end