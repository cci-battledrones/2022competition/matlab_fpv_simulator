function [pan, tilt] = estimatePanTilt(points)
%   =======================================================================
%  
%   Description:
%
%   This function estimates the pan and tilt angles from the current 
%   'look vector' of the drone to the provided points. The pan angle is
%   and angle from drone's 'look vector' to the projection of the point on 
%   the drone's local horizon and the tilt angle is the angle up or down 
%   from drone's look vector to the point. The function pulls the estimated
%   drone heading and the estimated location of the drone from the MATLAB
%   GLOBAL workspace and applies trigonometric identities to calculate the 
%   pan and tilt angle. 
%
%   Input Variables:
%       points - a list of row vectors from the map's origin to the 
%           desired point. 
%              Size: [numPoints x dim]
%                       'numPoints' corresponds to the number of points  
%                       'dim' corresponds to the number dimensions and is
%                        always equal to 3 
%              Units: meters
%              Reference Frame: Map's Global RFU
%
%%   Returned Variables:
%       pan - the angle (in degrees) across the drone's local horizon
%           (i.e., the Right-Front plane) from the drone's current 
%           'lookVector' to the a projection of the 'point' on to
%           the drone's local horizon. Positive angles are CCW 
%		    rotations (defined by the Right-hand Rule).
%               Size: ['numPoints' x 1]
%                   'numPoints' corresponds to the number of points 
%               Units: degrees
%       tilt - the angle off the drone's local horizon from the drone's 
%           current heading to the provided point in 3D space. Positive
%           angles are above the horizon (defined by the Right-hand Rule).
%               Size: ['numPoints' x 1]
%                   'numPoints' corresponds to the number of points 
%               Units: degrees
%
%   NOTE: Vec/norm(Vec) calculates a unit vector that carries the direction
%         of Vec
%   NOTE: The pan and tilt angles are measured relative to the current
%         'look vector' of the drone
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

% Step 1: Pulls the estimated location and estimated drone heading from Global Workspace
global estimatedDroneLocation estimatedDroneHeading 
    
    droneLocation = estimatedDroneLocation;
    lookVector = estimatedDroneHeading;
    
    numPoints = size(points,1);
    pan = zeros(numPoints,1);
    tilt = zeros(numPoints,1);
    for ii = 1:numPoints
% Step 2: Calculate a vector from the drone's estimated location to each point
        directionVector = (points(ii,:) - droneLocation)/norm(points(ii,:) - droneLocation);
% Step 3: Calculate pan and tilt angles from drone's current 'look vector' to each point
        [pan(ii), tilt(ii)] = calcPanTilt(lookVector, directionVector);
    end
end
