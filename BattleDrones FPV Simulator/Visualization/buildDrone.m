function buildDrone(droneLocation, lookVector, FOV)
%   =======================================================================
%   
%   Description:
%   This function is responsible for transforming the the first person view
%   figure into a FPV simulation where the camera of MATLAB axes can be 
%   moved and oriented to emulate the motion of FPV drone. This function 
%   places the camera in location prescribed in 'droneLocation' with the 
%   forward direction prescribed by 'lookVector.' 
%
%   Input Variables:
%       droneLocation - a vector of the drone's current location, points 
%                      from the origin of the map to the Drone.
%              Size: [1 x dimension]
%                    'dimension' corresponds to the right, forward, and up directions;
%                    it will always be 3.
%              Units: meters
%              Reference Frame: Map's Global RFU
%
%       lookVector - a unit vector of the drone's orientation, points 
%                along the FPV camera's boresight.
%              Size: [1 x dimension]
%                    'dimension' corresponds to the right, forward, and up directions;
%                    it will always be 3.
%               Reference Frame: Map's Global RFU
%
%        FOV - The field of view of the onboard camera
%              Size: [1 x 1]
%              Units: degrees
%
%   Returned Variables:
%       NONE
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
global fpvFigNum
    figure(fpvFigNum)
    set(gca, 'Projection','perspective')
    set(gca, 'CameraViewAngleMode','manual')
    set(gca, 'CameraViewAngle',FOV)
    set(gca,'CameraPosition',droneLocation)

    droneLocation = get(gca,'CameraPosition');
    set(gca,'CameraTarget',droneLocation + lookVector)             
    plotDrone()
end