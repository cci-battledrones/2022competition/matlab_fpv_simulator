function poseVector = getGatePose(gateID)
%   =======================================================================
%   
%   Description:
%   This function pulls information from the MATLAB GLOBAL workspace. It
%   finds the unit vector of the gate's orientation, which points along the
%   gate's normal vector. Unlike 'estimateGatePose', this function pulls 
%   the true gate orientation from the map. This is further different as it
%   reports the gate orientation in the Global coordinate frame, whereas 
%   'estimateGatePose' estimates the vector to the gate's orientation in 
%   the Drone's local right-forward-up coordinate frame (RFU)
%
%   It is expected that this function can be used to identify unknown gates 
%   or to derive the drone's current location based on its relative 
%   location to a known (i.e., identified) gate.
%
%   Input Variables:
%       gateID - Gate identification number, created in 'buildCourse'. A
%       gate's identification number dictates what order the drone will fly
%       through the gate
%              Size: [1 x 1]
%
%   Returned Variables:
%       poseVector - a unit vector normal to the plane of the gate. The 
%           vector points towards the viewer when viewed from the front
%           of the gate
%              Size: [1 x 3]
%              Units: unitless
%              Reference Frame: Map's Global RFU
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

    corners = getGateCorners(gateID);
    poseVector = calcPose(corners);
end