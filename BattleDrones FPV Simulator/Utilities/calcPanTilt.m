function [pan, tilt] = calcPanTilt(lookVector, pointVector)
%   =======================================================================
%   
%   Description:
%       This function calculates the pan and tilt angles of a point in 3D 
%       space from the drone's current forward direction. 
%
%   Input Variables:
%       lookVector - the drone's current heading vector.
%               Size: [1 x 3]
%                    Typically the lookVector will be [0 1 0].
%               Reference Frame: Drone's Local RFU 
%       pointVector - a vector from the drone to an arbitrary point in space
%               Size: [1 x 3]
%               Reference Frame: Drone's Local RFU 
%
%   Returned Variables:
%       pan - the angle (in degrees) across the drone's local horizon
%           (i.e., the Right-Front plane) from the drone's current 
%           'lookVector' to the a projection of the 'point' on to
%           the drone's local horizon. Positive angles are CCW 
%		    rotations (defined by the Right-hand Rule).
%               Size: ['numPoints' x 1]
%                   'numPoints' corresponds to the number of points 
%               Units: degrees
%       tilt - the angle off the drone's local horizon from the drone's 
%           current heading to the provided point in 3D space. Positive
%           angles are above the horizon (defined by the Right-hand Rule).
%               Size: [1 x 1]
%               Units: degrees
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

% Step 1: project vectors onto the local horizon plane
    projectedLookVector = [lookVector(1:2),0];
    projectedPointVector = [pointVector(1:2),0];

% Step 2: Calculate angle across the horizon
    pan = calcAngleBetweenVectors (projectedLookVector, projectedPointVector);

% Step 3: Calculate angle off the horizon    
    tilt = asind(pointVector(3));
end