function projectedCorners = getFrame()
%   =======================================================================
%   
%   Description:
%   This function emulates the output from the onboard computer vision. It
%   identifies the gates and their associated corners that are in view of
%   the camera. This function accesses information from the MATLAB GLOBAL 
%   workspace, but it does not return any information that should not be
%   know onboard the drone (i.e., corners and gate returned by this
%   function are disassociated from the corners and gate information in the
%   MAP).
%
%   It is expected that this function simulate the measurements that can be
%   made from the camera. 
%
%   Input Variables:
%       NONE
%
%   Returned Variables:
%       projectedCorners - Projected corners of the visible gates 
%              Size: [numCorners x panTilt x numGates]
%                       'numCorners' corresponds to the number of corners (corner
%                       number, clockwise starting at lower left corner,
%                       created in 'buildGate'). Typically there will always
%                       be 4 corners.
%                       'panTilt' will always have a dimension of two. Dimension
%                       one corresponds to the pan angles and dimension two to
%                       the tilt angles.
%                          NOTE: The first column  will always be the pan angle
%                          and the second will be tilt.
%                       'numGates' corresponds to the number of gates that are
%                       fully in the field of view of the camera. 
%              Units: degrees
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

global map fpvFigNum

% Step 1: Calculate the pan and tilt angles of all gate corners
    numberOfGates = length(map);
    fovAngle = get(gca(fpvFigNum), 'CameraViewAngle');
    for gateID = 1:numberOfGates
         [panAngles(:,gateID), tiltAngles(:,gateID)] = getPanTilt(getGateCorners(gateID));
    end
    
% Step 2: Create a Boolean list of visible corners
    isVisible = abs(panAngles)<fovAngle/2 & abs(tiltAngles)<fovAngle/2;
    fullyVisible = repmat(all(isVisible,1),[4 1]);
% Step 3: Return the corner pan and tilt angles of the fully visible gates    
    %NOTE: Assume the CV algorithm will not identify gate corners unless the
    %whole gate is in frame
    projectedCorners = [panAngles(fullyVisible), tiltAngles(fullyVisible)];
    %reshape array to fit expected output: [numCorners x panTilt x numGates]
    projectedCorners = [reshape(projectedCorners(:,1),4,1,[]),reshape(projectedCorners(:,2),4,1,[])];
end

