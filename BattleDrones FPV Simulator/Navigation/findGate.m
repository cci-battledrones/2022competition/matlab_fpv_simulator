function projectedCorners = findGate(gateID)
%   =======================================================================
%  
%   Description:
%   This returns the pan and tilt angles of a desired gate. Unlike 
%   'getGateCorners' and 'calcPanTilt' this function can only return the
%   the corners that are within the camera's field of view. If the desired
%   gate is not in view, the drone turns to try and put the desired gate
%   fully within the camera's field of view.
%
%   Input Variables:
%       gateID - Gate identification number, created in 'buildCourse'. A
%           gate's identification number dictates what order the drone will fly
%           through the gate
%              Size: [1 x 1]
%
%   Returned Variables:
%       projectedCorners - Projected corners of the desired gate
%              Size: [numCorners x panTilt]
%                       'numCorners' corresponds to the number of corners (corner
%                       number, clockwise starting at lower left corner,
%                       created in 'buildGate'). Typically there will always
%                       be 4 corners.
%                       'panTilt' will always have a dimension of two. Dimension
%                       one corresponds to the pan angles and dimension two to
%                       the tilt angles.
%              Units: degrees
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
    gateFound = 0;
    
    while not(gateFound)
        
% Step 1: Identify gates in drone's FOV
        [allProjectedCorners, gateIDs] = estimateGateIDs();
        
% Step 2: identify corners of desired gate if it is in view
        if any(gateID == gateIDs)
            gateIndex = find(gateID == gateIDs);
            projectedCorners = allProjectedCorners(:,:,gateIndex);
            gateFound = 1;
        
%Step 3: if the desired gate is not in view, turn the drone to face the desired gate
        else
            searchForGate(gateID) 
        end
    end
end