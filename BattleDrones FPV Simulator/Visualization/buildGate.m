function corners = buildGate()
%   =======================================================================
%   
%   Description:
%   This function generates a generic 3D representation  of a gate for the 
%   BattleDrone's obstacle course. The gate consist of four corners of a 
%   specific size. The corner locations a placed in the XZ plane and 
%   centered about the origin so they can be rotated and translated directly
%   into their place on the course map. 
%
%   For Example: gatesCorners = corners*rotateVector3(angle) + [gateLocation];
%
%   Input Variables:
%       NONE
%
%   Returned Variables:
%       corners - a list of vectors pointing to the locations of a gate's corners
%              Size: [numCorners x dimension] 
%                    where numCorners is number of gate corners  
%                    and 'dimension' corresponds to the right, forward, and 
%                    up directions and will always have a length of 3.
%              Units: meters
%              Reference Frame: Map's Global RFU
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================
global  sideLength
%create gate template
    gateType = [0,0,0;
                0,0,1;
                1,0,1;
                1,0,0]*sideLength;
    %center template on origin
    corners = gateType  - [1;1;1;1]*[1,0,1]*sideLength/2;  
end