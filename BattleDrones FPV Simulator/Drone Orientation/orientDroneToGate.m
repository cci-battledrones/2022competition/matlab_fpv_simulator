function [droneRelativeLocation, droneRelativeOrientation] = orientDroneToGate(projectedCorners)

%   =======================================================================
%  
%   Description:
%   This function takes in the pan and tilt corners of a gate and will return
%   the Drone's location and orientation in a gate-centric frame. It is 
%   expected that this function will be used in conjunction with 
%   'orientDroneToMap' to estimate the drone's location within the Map.
%
%   Input Variables:
%       projectedCorners - Projected corners of the gate 
%              Size: [numCorners x panTilt]
%                       'numCorners' corresponds to the number of corners (corner
%                       number, clockwise starting at lower left corner,
%                       created in 'buildGate'). Typically there will always
%                       be 4 corners.
%                       'panTilt' will always have a dimension of two. Dimension
%                       one corresponds to the pan angles and dimension two to
%                       the tilt angles.
%                       NOTE: The first column will always be the pan angle
%                       and the second will be tilt.
%              Units: degrees
%
%   Returned Variables:
%       droneRelativeLocation - A vector from the center of the gate to the Drone
%              Size: [1 x dimension]
%                    'dimension' corresponds to the right, forward, and up directions;
%                    it will always be 3.
%              Units: Meters
%              Reference Frame: Gate's Local RFU
%       droneRelativeOrientation -  a unit vector of the drone's orientation, points 
%               along the FPV camera's boresight as viewed from the gate.
%              Size: [1 x dimension]
%                    'dimension' corresponds to the right, forward, and up directions;
%                    it will always be 3.
%              Units: unitless
%              Reference Frame: Gate's Local RFU
%
%   Author: Kevin Schroeder, PhD
%   Date:  31 May 2021
%
%   =======================================================================

%Step 1: Estimate gate's location and orientation
    droneLookVector = [0,1,0];
    gateRelativeLocation = estimateGateLocation(projectedCorners);
    gatePose = estimateGatePose(projectedCorners); 
%Step 2: Design a frame transformation from the drone frame to the gate frame
    drone2GateAngularOffset = calcAngleBetweenVectors (droneLookVector, gatePose);
    drone2Gate = rotateFrame3(drone2GateAngularOffset);
%Step 3: Convert Drone measurements to a local gate-based frame
    %NOTE: vecDrone2Gate = -1 * vecGate2Drone ---------
    droneRelativeLocation = -gateRelativeLocation*drone2Gate;
    droneRelativeOrientation = droneLookVector*drone2Gate;
end